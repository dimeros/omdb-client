import { Injectable } from '@angular/core';
import { MovieSearch, Movie, MovieDetails } from './movie.model';

import {HttpClient} from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

    API_KEY: string = '1c153b69';
    constructor(private httpClient : HttpClient) { }

  getMovies(name: string, plot: string): Observable<MovieSearch> {
    return this.httpClient.get<MovieSearch>(`http://www.omdbapi.com/?apikey=${this.API_KEY}&s=${name}&plot=${plot}`);
  }

  getMovie(id: string): Observable<MovieDetails> {
    return this.httpClient.get<MovieDetails>(`http://www.omdbapi.com/?apikey=${this.API_KEY}&i=${id}`);
  }

  // getMovies(name: string, plot: string): Observable<MovieSearch> {
  //   let data = {
  //     "Search": [
  //         {
  //             "Title": "The Exorcist",
  //             "Year": "1973",
  //             "imdbID": "tt0070047",
  //             "Type": "movie",
  //             "Poster": "https://m.media-amazon.com/images/M/MV5BYjhmMGMxZDYtMTkyNy00YWVmLTgyYmUtYTU3ZjcwNTBjN2I1XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg"
  //         },
  //         {
  //             "Title": "Exorcist: The Beginning",
  //             "Year": "2004",
  //             "imdbID": "tt0204313",
  //             "Type": "movie",
  //             "Poster": "https://m.media-amazon.com/images/M/MV5BMjI0NzY1MzQ0MV5BMl5BanBnXkFtZTcwOTM1MTEzMw@@._V1_SX300.jpg"
  //         },
  //         {
  //             "Title": "The Exorcist III",
  //             "Year": "1990",
  //             "imdbID": "tt0099528",
  //             "Type": "movie",
  //             "Poster": "https://m.media-amazon.com/images/M/MV5BZDEwMmYxM2MtZGQyMy00NTRjLWJjMTMtYWM1NWU1ZWNhZTY0XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg"
  //         },
  //         {
  //             "Title": "The Exorcist",
  //             "Year": "2016–2018",
  //             "imdbID": "tt5368542",
  //             "Type": "series",
  //             "Poster": "https://m.media-amazon.com/images/M/MV5BMjEzNjI5Njg4MV5BMl5BanBnXkFtZTgwMjkwMjU2MzI@._V1_SX300.jpg"
  //         },
  //         {
  //             "Title": "Exorcist II: The Heretic",
  //             "Year": "1977",
  //             "imdbID": "tt0076009",
  //             "Type": "movie",
  //             "Poster": "https://m.media-amazon.com/images/M/MV5BZTQ2NmY5NmUtNWJiNS00ZjZmLTllMjQtNzU3YWZlM2E2MmJjXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg"
  //         },
  //         {
  //             "Title": "Dominion: Prequel to the Exorcist",
  //             "Year": "2005",
  //             "imdbID": "tt0449086",
  //             "Type": "movie",
  //             "Poster": "https://m.media-amazon.com/images/M/MV5BODVlMzg1NDQtNjU3MC00Mjk0LThhN2ItZmVmZGIwMjYyZmYyXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg"
  //         },
  //         {
  //             "Title": "Blue Exorcist: The Movie",
  //             "Year": "2012",
  //             "imdbID": "tt3028018",
  //             "Type": "movie",
  //             "Poster": "https://m.media-amazon.com/images/M/MV5BMjA5OTgwNDU4MV5BMl5BanBnXkFtZTcwMDcwMTI4OQ@@._V1_SX300.jpg"
  //         },
  //         {
  //             "Title": "Shark Exorcist",
  //             "Year": "2015",
  //             "imdbID": "tt3120314",
  //             "Type": "movie",
  //             "Poster": "https://m.media-amazon.com/images/M/MV5BYzc1Mzg3ZmEtZDkzYy00YWJmLThhY2UtZTRmOWZiYTRiYTY1XkEyXkFqcGdeQXVyNTQ2OTMwMjc@._V1_SX300.jpg"
  //         },
  //         {
  //             "Title": "Exorcist: House of Evil",
  //             "Year": "2016",
  //             "imdbID": "tt4864584",
  //             "Type": "movie",
  //             "Poster": "https://m.media-amazon.com/images/M/MV5BOTcyMGM2N2QtNGExYS00Y2ZiLWJhY2QtYjUwNTJhNzk4OGRiXkEyXkFqcGdeQXVyMzQxNzMxMA@@._V1_SX300.jpg"
  //         },
  //         {
  //             "Title": "Anneliese: The Exorcist Tapes",
  //             "Year": "2011",
  //             "imdbID": "tt1911533",
  //             "Type": "movie",
  //             "Poster": "https://m.media-amazon.com/images/M/MV5BMjE1MTk5NTE0N15BMl5BanBnXkFtZTcwNjQyNTA5Ng@@._V1_SX300.jpg"
  //         }
  //     ],
  //     "totalResults": 74,
  //     "Response": true,
  //     //"Error": "error here"
  //   };

  //   return of(data);
  // }
}
