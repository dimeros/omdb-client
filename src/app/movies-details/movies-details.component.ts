import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie.service';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { Movie, MovieDetails } from '../movie.model';

@Component({
  selector: 'app-movies-details',
  templateUrl: './movies-details.component.html',
  styleUrls: ['./movies-details.component.css']
})
export class MoviesDetailsComponent implements OnInit {
  errorMessage: string;
  movie: MovieDetails = <MovieDetails>{};

  constructor(private movieService: MovieService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params
      .pipe(map(param => param.id), switchMap(id => this.movieService.getMovie(id)))
      .subscribe((movieDetails: any) => {

        // check if error exists
        if (movieDetails.Error) {
          this.errorMessage = movieDetails.Error;
        } else {
          this.movie = movieDetails;
        }
        console.log(this.movie, this.errorMessage);
      }, (error: any) => {
        this.errorMessage = error;
      })
  }

  back():void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
