export class Movie {
  public Title:string;
  public Year:string;
  public imdbID:string;
  public Type:string;
  public Poster:string;
}

export class Ratings {
  public Source: string;
  public Value: string;
}

export class MovieDetails extends Movie {
  public Rated: string;
  public Released: string;
  public Runtime: string;
  public Genre: string;
  public Director: string;
  public Writer: string;
  public Actors: string;
  public Plot: string;
  public Language: string;
  public Country: string;
  public Awards: string;
  public Poster: string;
  public Ratings: Ratings[];
  public Metascore: string;
  public imdbRating: string;
  public imdbVotes: string;
  public imdbID: string;
  public Type: string;
  public DVD: string;
  public BoxOffice: string;
  public Production: string;
  public Website: string;

}

export class MovieSearch {
  public Search: Movie[];
  public totalResults: number;
  public Response: boolean;
  public Error?: string;
}