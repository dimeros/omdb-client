import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  darkMode : boolean = false;
  constructor() { }

  changeMode(): void {
      this.darkMode = !this.darkMode;
      this.changeBody(this.darkMode);
      localStorage.setItem('darkMode', `${this.darkMode}`);

  }
  ngOnInit(): void {
    this.darkMode = (localStorage.getItem('darkMode') === 'true');
    this.changeBody(this.darkMode);
  }

  /**
   * Change body based on dark mode
   * @param darkMode the dark mode defined
   */
  private changeBody(darkMode: boolean):void {
    let body = document.getElementsByTagName('body')[0];
    if (darkMode) {
      body.classList.add('dark-mode');         
    } else {
      body.classList.remove('dark-mode');         
    }
  }
}
