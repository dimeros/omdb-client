
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MovieService } from '../movie.service';
import { Movie, MovieSearch } from '../movie.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Observable, BehaviorSubject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit, OnDestroy {
  searchSub: Subscription;
  movies$: Subject<Movie[]> = new Subject<Movie[]>();
  errorMessage$: BehaviorSubject<string> = new BehaviorSubject<string>("");
  //movies: Movie[];

  title = 'omdb-client';
  searchName: string = '';
  searchPlot: string = '';

  private searchTerms = new Subject<string>();
  // errorMessage: string;
  plots = [{name: 'short'}, {name: 'full'}];
  // plots:String[] = ['Short', 'Full'];

  constructor(private movieService: MovieService,  private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {

    let searchSupscription: Observable<string> = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),
    
      // ignore new term if same as previous term
      distinctUntilChanged(),
    
    );
    
    this.searchSub = searchSupscription.subscribe((term: string ) => {
      this.onSearch(term, this.searchPlot);
    }, (error) => {
      this.errorMessage$.next(error.message);
    });

    this.onSearch("", this.searchPlot);
  }

  ngOnDestroy(): void {
    this.searchSub.unsubscribe();
  }


  onSearchCategory(newCategory: any): void {
    this.searchPlot = newCategory;
    this.onSearch(this.searchName, this.searchPlot);
  }
  onSearchName(term:string) {
    this.searchTerms.next(term);
  }

  onViewDetails(id: any) {
    this.router.navigate([id], {relativeTo: this.route});
  }

  private onSearch(name:string, plot: string):void {
    if (name === "") {
      this.errorMessage$.next("Please set an item to search movie");
      return;
    }
    this.errorMessage$.next("");
    this.movieService.getMovies(name, plot).subscribe( (movieSearchResult:MovieSearch) => {
      // check if error exists
      if (movieSearchResult.Error) {
        this.errorMessage$.next(movieSearchResult.Error);
      } else {
        this.movies$.next(movieSearchResult.Search);
      }
    });
  }
}
